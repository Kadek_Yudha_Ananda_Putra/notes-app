// const initialData = JSON.parse(localStorage.getItem("NOTES_APP"))
//   ? JSON.parse(localStorage.getItem("NOTES_APP"))
//   : [];

// const getInitialData = () => initialData;

const getInitialData = () => [
  {
    "id": 1,
    "title": "Webpack",
    "body": "Webpack adalah alat sumber terbuka yang digunakan untuk mengelola dependensi dan menggabungkan berkas JavaScript. Ini sering digunakan untuk membangun proyek JavaScript modern, mengelola modul, dan mengoptimalkan kinerja.",
    "createdAt": "2023-11-02T12:00:00.000Z",
    "archived": false
  },
  {
    "id": 2,
    "title": "Functional Components in React",
    "body": "Komponen fungsional adalah tipe komponen dalam React yang dibuat menggunakan fungsi JavaScript. Mereka mengembalikan elemen React dan digunakan dengan cara yang mirip dengan komponen kelas dalam React.",
    "createdAt": "2023-11-02T12:00:00.000Z",
    "archived": false
  },
  {
    "id": 3,
    "title": "Modularization in JavaScript",
    "body": "Modularisasi dalam pemrograman JavaScript adalah pendekatan untuk memecah kode ke dalam modul terpisah berdasarkan fungsionalitas mereka. Ini membantu dalam pengelolaan dan pemeliharaan kode yang lebih baik.",
    "createdAt": "2023-11-02T12:00:00.000Z",
    "archived": false
  },
  {
    "id": 4,
    "title": "React Component Lifecycle",
    "body": "Siklus hidup komponen dalam React adalah serangkaian metode yang dipanggil saat komponen dibuat, dirender, dipasang, dan lainnya. Ini penting dalam mengelola perilaku komponen dalam aplikasi React.",
    "createdAt": "2023-11-02T12:00:00.000Z",
    "archived": false
  },
  {
    "id": 5,
    "title": "ECMAScript Modules (ESM)",
    "body": "Modul ECMAScript (ESM) adalah format standar untuk modularisasi kode JavaScript. Mereka memungkinkan untuk mengimpor dan mengekspor berbagai bagian kode antar berkas dengan cara yang terstruktur.",
    "createdAt": "2023-11-02T12:00:00.000Z",
    "archived": false
  },
  {
    "id": 6,
    "title": "JavaScript Module Bundlers",
    "body": "Pustaka pengikat modul dalam JavaScript adalah alat yang digunakan untuk menggabungkan semua modul yang digunakan dalam aplikasi menjadi satu berkas tunggal. Ini berguna dalam mengelola dependensi dan mempercepat waktu pemuatan aplikasi.",
    "createdAt": "2023-11-02T12:00:00.000Z",
    "archived": false
  }  
];

const showFormattedDate = (date) => {
  const options = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  };
  return new Date(date).toLocaleDateString("en-US", options);
};

export { getInitialData, showFormattedDate };
